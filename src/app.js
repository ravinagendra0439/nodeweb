const express = require('express');

const app = express();

app.get('/', (req, res) => {
    res.send(`
        <html>
            <body>
                <h1> Hello Node web </h1>
            </body>
        </html>
    `);
});

const port = process.env.PORT || 3000;

app.listen(port, () => console.log('server running on port ' + port));
